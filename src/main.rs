#[cfg(test)]
mod tests;
mod types;

use crate::types::*;
use read_input::prelude::*;

fn get_coords(x_limit: usize, y_limit: usize) -> (i32, i32) {
    let x = input::<i32>()
        .msg("x coordinate: ")
        .err(format!(
            "Please input a positive integer between 0 and {}",
            x_limit - 1
        ))
        .inside(0..(x_limit as i32))
        .get();
    let y = input::<i32>()
        .msg("y coordinate: ")
        .err(format!(
            "Please input a positive integer between 0 and {}",
            y_limit - 1
        ))
        .inside(0..(y_limit as i32))
        .get();
    (x, y)
}

fn get_ship_inputs(x_limit: usize, y_limit: usize) -> Battleship {
    println!("head of ship coordinates: ");
    let head = get_coords(x_limit, y_limit);

    let length = input::<i32>()
        .msg("length of ship: ")
        .err("please give a positive integer between 2 and 5")
        .inside(2..=5)
        .get();

    let dir = input::<Direction>()
        .msg("direction of ship: ")
        .err("please give N, S, E, W")
        .get();

    Battleship::new(head, length, dir)
}

fn take_shot(board: &mut GameBoard, coord: (i32, i32)) {
    for ship in &mut board.ships {
        if ship.body.contains(&coord) {
            board.board[coord.0 as usize][coord.1 as usize] = Space::Hit;
            ship.health -= 1;
            return;
        }
    }

    board.board[coord.0 as usize][coord.1 as usize] = Space::Miss;
}

fn check_win(board: &GameBoard) -> bool {
    let mut total_health = 0;
    for ship in &board.ships {
        total_health += ship.health;
    }
    return total_health == 0;
}

fn setup_game() -> (GameBoard, GameBoard) {
    println!("Please input the dimensions of the board: ");
    let width = input::<usize>()
        .msg("width: ")
        .err("Please input a positive integer greater than 3")
        .add_test(|x| x > &3)
        .get();
    let height = input::<usize>()
        .msg("height: ")
        .err("Please input a positive integer greater than 3")
        .add_test(|x| x > &3)
        .get();
    let num_ships = input::<usize>()
        .msg("How many ships: ")
        .err("Please input a positive integer less than 6")
        .inside(0..6)
        .get();

    let mut board1 = GameBoard::new(width, height);
    let mut board2 = GameBoard::new(width, height);

    println!("{}[2J", 27 as char);

    input::<String>()
        .msg("Player 1, time to place your ships\n Enter to continue")
        .get();

    while board1.ships.len() < num_ships {
        print!("{}[2J", 27 as char);
        println!("Current Placement of Ships");
        board1.ship_render();
        let ship = get_ship_inputs(board1.width, board1.height);
        board1.add_ship(ship).unwrap_or_else(|e| println!("{}", e));
    }
    board1.ship_render();

    input::<String>()
        .msg("Here are your ship placements (enter to continue)")
        .get();

    println!("{}[2J", 27 as char);

    input::<String>()
        .msg("Player 2, time to place your ships\n Enter to continue")
        .get();

    while board2.ships.len() < num_ships {
        println!("{}[2J", 27 as char);
        println!("Current Placement of Ships");
        board2.ship_render();
        let ship = get_ship_inputs(board2.width, board2.height);
        board2.add_ship(ship).unwrap_or_else(|e| println!("{}", e));
    }
    board2.ship_render();

    input::<String>()
        .msg("Please confirm this placement (press enter)")
        .get();

    (board1, board2)
}

fn main() {
    let (mut player1, mut player2) = setup_game();

    let mut current_player = 1;

    while !check_win(&player1) && !check_win(&player2) {
        println!("{}[2J", 27 as char);
        match current_player {
            1 => player1.dual_render(&player2),
            2 => player2.dual_render(&player1),
            _ => {}
        }

        println!("Player {}, where would you like to shoot?", current_player);
        let (x, y) = get_coords(player1.width, player1.height);
        match current_player {
            1 => {
                take_shot(&mut player2, (x, y));
                current_player = 2
            }
            2 => {
                take_shot(&mut player1, (x, y));
                current_player = 1
            }
            _ => {}
        }
    }

    match current_player {
        1 => println!("Player 2 Wins!"),
        2 => println!("Player 1 Wins!"),
        _ => {}
    }
}
