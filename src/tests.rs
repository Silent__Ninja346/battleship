use super::*;

#[test]
fn test_contstruct_ship() {
    let head = (1, 1);
    let length = 3;
    let direction = Direction::South;
    let ship = Battleship::new(head, length, direction);
    assert_eq!(ship.body, vec![(1, 1), (1, 2), (1, 3)]);
}

#[test]
fn test_valid_ship() {
    let board = GameBoard::new(10, 10);
    let invalid_ship = Battleship::new((1, 1), 3, Direction::North);
    let valid_ship = Battleship::new((1, 1), 3, Direction::South);
    assert_eq!(board.is_valid_ship(&invalid_ship), false);
    assert_eq!(board.is_valid_ship(&valid_ship), true);
}

#[test]
fn test_add_ship() {
    let mut board = GameBoard::new(10, 10);
    let invalid_ship = Battleship::new((1, 1), 3, Direction::North);
    let valid_ship = Battleship::new((1, 1), 3, Direction::South);
    match board.add_ship(invalid_ship) {
        Ok(_) => {}
        Err(e) => {
            println!("{}", e)
        }
    }
    match board.add_ship(valid_ship) {
        Ok(_) => {}
        Err(e) => {
            println!("{}", e)
        }
    }
    let valid_ship = Battleship::new((1, 1), 3, Direction::South);
    assert_eq!(board.ships, vec![valid_ship]);
}

#[test]
fn test_take_shot() {
    let mut board = GameBoard::new(10, 10);
    let ship1 = Battleship::new((1, 1), 3, Direction::South);
    let ship2 = Battleship::new((5, 6), 2, Direction::South);
    board.add_ship(ship1).unwrap();
    board.add_ship(ship2).unwrap();
    take_shot(&mut board, (1, 1));
    take_shot(&mut board, (1, 2));
    take_shot(&mut board, (5, 6));
    take_shot(&mut board, (5, 7));
    take_shot(&mut board, (5, 5));
    take_shot(&mut board, (9, 9));
    assert_eq!(board.board[1][1], Space::Hit);
    assert_eq!(board.board[1][2], Space::Hit);
    assert_eq!(board.board[5][6], Space::Hit);
    assert_eq!(board.board[5][7], Space::Hit);
    assert_eq!(board.board[5][5], Space::Miss);
    assert_eq!(board.board[9][9], Space::Miss);
}

#[test]
fn test_parse_direction() {
    let successful_parse: Direction = "S".parse().unwrap();
    let failed_parse = "k".parse::<Direction>().unwrap_err();
    assert_eq!(successful_parse, Direction::South);
    assert_eq!(failed_parse, ParseDirectionError);
}
